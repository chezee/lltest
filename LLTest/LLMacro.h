#define LLEmpty

#define kLLStringVariableDefinition(variable, value)   static NSString * const variable = value;

#define LLWeakify(variable) \
__weak __typeof(variable) __ZHWeakified_##variable = variable;

#define LLStrongify(variable) \
__strong __typeof(variable) variable = __ZHWeakified_##variable;

#define LLStrongifyAndReturnIfNil(variable) \
LLStrongifyAndReturnResultIfNil(variable, LLEmpty)

#define LLStrongifyAndReturnNilIfNil(variable) \
LLStrongifyAndReturnResultIfNil(variable, nil)

#define LLStrongifyAndReturnResultIfNil(variable, result) \
    LLStrongify(variable); \
    if (!variable) { \
        return result; \
    }
