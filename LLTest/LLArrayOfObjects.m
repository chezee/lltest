#import "LLArrayOfObjects.h"
#import "LLFetchData.h"
#import "LLObject.h"
#import "LLMacro.h"

@interface LLArrayOfObjects ()
@property (nonatomic, strong) NSMutableArray *arrayOfObjects;

@end

@implementation LLArrayOfObjects

#pragma mark --Initializers

- (instancetype) init {
    self = [super init];
    if (self) {
        self.arrayOfObjects = [NSMutableArray array];
    }
    
    return self;
}

- (NSArray *) objects {
    @synchronized (self) {
        return [self.arrayOfObjects mutableCopy];
    }
}

- (NSInteger) countOfObjects {
    @synchronized (self) {
        return self.arrayOfObjects.count;
    }
}

#pragma mark --Making changes to array

- (void)addObject:(id)object {
    if (!object) {
        return;
    }
    
    @synchronized (self) {
        [self.arrayOfObjects addObject:object];
    }
}

- (void)removeObject:(id)object {
    if (!object) {
        return;
    }
    
    @synchronized (self) {
        [self.arrayOfObjects removeObject:object];
    }
}

- (void)addObjects:(NSArray *)objects {
    @synchronized (self) {
        for (id object in objects) {
            [self addObject:object];
        }
    }
}

- (id)objectAtIndex:(NSUInteger)index {
    if (!(index < [self countOfObjects])) {
        return nil;
    }
    
    @synchronized (self) {
        return self.arrayOfObjects[index];
    }
}

- (void)insertObject:(id)object atIndex:(NSUInteger)index {
    if (!object) {
        return;
    }
    
    @synchronized (self) {
        [self.arrayOfObjects insertObject:object atIndex:index];
        }
}

- (BOOL)containsObject:(id)object {
    @synchronized (self) {
        return [self.arrayOfObjects containsObject:object];
    }
}

- (void)removeObjectAtIndex:(NSUInteger)index {
    @synchronized (self) {
        [self.arrayOfObjects removeObjectAtIndex:index];
    }
}

- (NSInteger)indexOfObject:(id)object {
    @synchronized (self) {
        return [self.arrayOfObjects indexOfObject:object];
    }
}

- (id)objectAtIndexedSubscript:(NSUInteger)index {
    return [self objectAtIndex:index];
}

#pragma mark --NSFastEnumerating implementation

- (NSUInteger)countByEnumeratingWithState:(NSFastEnumerationState *)state
                                  objects:(id __unsafe_unretained [])buffer
                                    count:(NSUInteger)length
{
    return [self.arrayOfObjects countByEnumeratingWithState:state objects:buffer count:length];
}

@end
