#import <Foundation/Foundation.h>

@interface LLObject : NSObject
@property (nonatomic, copy) NSString    *ID;
@property (nonatomic, copy) NSString    *link;
@property (nonatomic, copy) NSString    *name;
@property (nonatomic, copy) NSDate      *date;
@property (nonatomic, copy) NSString    *imageLink;
@property (nonatomic)       BOOL        top;

- (instancetype)initWithDict:(NSDictionary *)dict;

@end
