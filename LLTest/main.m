//
//  main.m
//  LLTest
//
//  Created by Ilya on 18/5/17.
//  Copyright © 2017 Ilya. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LLAppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([LLAppDelegate class]));
    }
}
