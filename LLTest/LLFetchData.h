#import <Foundation/Foundation.h>

@interface LLFetchData : NSObject
typedef void(^LLCompletionBlock)(NSData *data, NSURLResponse *response, NSError *error);

+ (void)fetchDataFromURL:(NSString *)url withCompletion:(LLCompletionBlock)completion;


@end
