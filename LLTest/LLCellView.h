#import <UIKit/UIKit.h>
#import "LLObject.h"

@interface LLCellView : UITableViewCell
@property (nonatomic, strong)        LLObject       *object;
@property (weak, nonatomic) IBOutlet UIImageView *previewImage;
@property (weak, nonatomic) IBOutlet UILabel *date;
@property (weak, nonatomic) IBOutlet UILabel *source;
@property (weak, nonatomic) IBOutlet UITextField *titleText;

@end
