#import "LLFetchData.h"
#import "LLMacro.h"

@implementation LLFetchData

+ (void) fetchDataFromURL:(NSString *)url withCompletion:(LLCompletionBlock)completion {
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:completion];
    [dataTask resume];
}

@end
