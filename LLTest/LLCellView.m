#import "LLCellView.h"

@implementation LLCellView

- (void)setObject:(LLObject *)object {
    NSString *source = [NSString stringWithString:object.link];
    source = [source stringByReplacingCharactersInRange:NSMakeRange(0, 7) withString:@""];
    self.source.text = source;
    self.titleText.text = object.name;
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"yyyy mm dd"];
    self.date.text = [formatter stringFromDate:object.date];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        NSURL *url = [[NSURL alloc] initWithString:object.imageLink];
        NSData *data = [[NSData alloc] initWithContentsOfURL:url];
        UIImage *image = [UIImage imageWithData:data];
        dispatch_async(dispatch_get_main_queue(), ^{
            self.previewImage.image = image;
        });
    });
    NSArray *subViews = @[self.previewImage, self.source, self.date, self.titleText];
    for (UIView *item in subViews) {
        [self addSubview:item];
    }
}

@end
