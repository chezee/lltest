#import <Foundation/Foundation.h>

@interface LLArrayOfObjects : NSObject <NSFastEnumeration>
@property (nonatomic, readonly) NSArray *objects;

- (void)addObject:(id)model;
- (void)removeObject:(id)model;

- (void)addObjects:(NSArray *)models;

- (id)objectAtIndex:(NSUInteger)index;

- (BOOL)containsObject:(id)model;
- (NSInteger)indexOfObject:(id)model;
- (NSInteger) countOfObjects;

- (id)objectAtIndexedSubscript:(NSUInteger)index;

@end
