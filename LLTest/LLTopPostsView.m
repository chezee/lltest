#import "LLTopPostsView.h"

@implementation LLTopPostsView

- (void) setObject:(LLObject *)object {
    self.sourceLabel.text = object.link;
    self.titleText.text = object.name;
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"yyyy mm dd"];
    self.dateLabel.text = [formatter stringFromDate:object.date];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        NSURL *url = [[NSURL alloc] initWithString:object.imageLink];
        NSData *data = [[NSData alloc] initWithContentsOfURL:url];
        UIImage *image = [UIImage imageWithData:data];
        dispatch_async(dispatch_get_main_queue(), ^{
            self.previewImage.image = image;
        });
    });

}

@end
