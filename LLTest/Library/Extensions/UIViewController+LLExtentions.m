#import "UIViewController+LLExtentions.h"

@implementation UIViewController (LLExtensions)

- (id) initWithNib {
    NSString *nibName = [NSString stringWithFormat:@"%@", self.class];
    NSBundle *bundle = nil;
    id controller = [self initWithNibName:nibName bundle:bundle];
    
    return controller;
}

@end
