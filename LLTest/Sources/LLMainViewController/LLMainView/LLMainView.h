#import <UIKit/UIKit.h>

@interface LLMainView : UIView
@property (weak, nonatomic) IBOutlet UITableView *table;
@property (weak, nonatomic) IBOutlet UIToolbar *toolBar;
@property (weak, nonatomic) IBOutlet UITabBar *tabBar;

@end
