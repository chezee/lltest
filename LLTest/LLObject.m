#import "LLObject.h"
#import "LLMacro.h"
#import "LLFetchData.h"

kLLStringVariableDefinition(ID, @"id");
kLLStringVariableDefinition(linkToPost, @"link");
kLLStringVariableDefinition(name, @"name");
kLLStringVariableDefinition(date, @"date");
kLLStringVariableDefinition(imageLink, @"cover");
kLLStringVariableDefinition(isTop, @"top");

@implementation LLObject

- (instancetype)initWithDict:(NSDictionary *)dict{
    self = [super init];
    if(self) {
        self.ID = dict[ID];
        self.link = dict[linkToPost];
        NSString *dateString = dict[date];
        NSDate *date = [[NSDate alloc] initWithTimeIntervalSince1970:dateString.longLongValue];
        self.date = date;
        self.name = dict[name];
        self.imageLink = dict[imageLink];
        self.top = [dict[isTop] boolValue];
    }
    return self;
}

@end
