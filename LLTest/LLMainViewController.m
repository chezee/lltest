#import "LLMainViewController.h"
#import "LLCellView.h"
#import "LLTopPostsView.h"
#import "LLMacro.h"
#import "LLArrayOfObjects.h"
#import "LLFetchData.h"

kLLStringVariableDefinition(cellIdentifier, @"CellIdentifier")
kLLStringVariableDefinition(cellView, @"LLCellView")

kLLStringVariableDefinition(targetLink, @"http://owledge.ru/api/v1/feedNews?lang=en&count=10&sources=7,19,13,5,15,16,12,9,10012,10010,10013,10014,10019,10018,10011&feedLineId=5")

@interface LLMainViewController () {
    LLArrayOfObjects *topPosts;
    LLArrayOfObjects *postsArray;
    NSArray          *array;

    LLTopPostsView *topView;
}

@end

@implementation LLMainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    LLMainView *mainView = self.mainView;
    
    [mainView.table registerNib:[UINib nibWithNibName:cellView bundle:nil]
         forCellReuseIdentifier:cellIdentifier];

    topPosts = [LLArrayOfObjects new];
    postsArray = [LLArrayOfObjects new];

    [LLFetchData fetchDataFromURL:targetLink withCompletion:^(NSData *data, NSURLResponse *response, NSError *error) {
         if (data && !error) {
             array = [NSJSONSerialization JSONObjectWithData:data
                                                     options:0
                                                       error:nil];
             if (array) {
                 for (int i = 0; i < [array count]; i++) {
                     LLObject *object = [[LLObject alloc] initWithDict:array[i]];
                     if (object.top){
                         [topPosts addObject:object];
                     } else {
                         [postsArray addObject:object];
                     }
                 }
                 [mainView.table reloadData];
             }
         }
     }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [postsArray countOfObjects];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    LLCellView *cell = [self.mainView.table dequeueReusableCellWithIdentifier:cellIdentifier];
    cell.object = [postsArray objects][indexPath.row];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 222;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 270;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    topView = [[LLTopPostsView alloc] init];
    UIScrollView *scroll = [[UIScrollView alloc] init];
    CGSize scrollSize = scroll.frame.size;
    
    for (int i = 0; i < [topPosts countOfObjects]; i++) {
        topView.frame = CGRectMake(scrollSize.width * i, 0, scrollSize.width, scrollSize.height);
        [topView setObject:topPosts[i]];
        [scroll addSubview:topView];
    }
    scroll.contentSize = CGSizeMake(scrollSize.width * [topPosts countOfObjects], scrollSize.height);
    scroll.pagingEnabled = YES;
    
    return scroll;
}

@end
