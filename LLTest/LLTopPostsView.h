#import <UIKit/UIKit.h>
#import "LLObject.h"

@interface LLTopPostsView : UIView
@property (nonatomic, strong) LLObject *object;

@property (weak, nonatomic) IBOutlet UIImageView *previewImage;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;

@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *sourceLabel;
@property (weak, nonatomic) IBOutlet UILabel *topIndicatorLabel;

@property (weak, nonatomic) IBOutlet UITextField *titleText;


@end
