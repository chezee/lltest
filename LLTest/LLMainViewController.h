#import <UIKit/UIKit.h>
#import "LLMainView.h"

@interface LLMainViewController : UIViewController <UITableViewDelegate,
                                                    UITableViewDataSource>
@property (strong, nonatomic) IBOutlet LLMainView *mainView;



@end

